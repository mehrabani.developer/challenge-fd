<?php

namespace App\Http\Controllers\Api\V1\Search;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        if(! $query = $request->query->get('keyword')) {
            return $this->errorResponse("'keyword' parameter is required", Response::HTTP_BAD_REQUEST);
        }

        try {
            $results = Cache::remember('search_results_'.$query, config('cache.lifetime'), function () use($query) {
                return Http::timeout(5)->post('https://search.fidibo.com?q=' . $query)
                        ->json()['books']['hits']['hits'];
            });

            $sources = $this->normalizeData($results);
            return $this->successResponse($sources);

        } catch (\Exception $e) {
            Log::error($e);
                                        /*'get data failed'*/
            return $this->errorResponse($e->getMessage(), $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    protected function normalizeData($results) {
        $hits = Arr::pluck($results, '_source') ?: [];

        return array_map(function ($source) {
            unset($source['weight']);
            return $source;
        }, $hits);
    }
}
