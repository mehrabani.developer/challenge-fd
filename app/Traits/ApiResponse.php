<?php

namespace App\Traits;

trait ApiResponse
{
    public function successResponse($data = null, $message = null, $statusCode = 200)
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $data
        ],$statusCode);
    }

    public function errorResponse($message = null, $statusCode = 500, $data = null)
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
            'data' => $data
        ],$statusCode);
    }
}
