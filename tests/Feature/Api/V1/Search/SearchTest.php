<?php

namespace Tests\Feature\Api\V1\Search;

use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SearchTest extends TestCase
{
    public function test_successful_search()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => 'password'
        ]);

        $token = $response->decodeResponseJson()['data']['access_token'];

        $response->assertStatus(Response::HTTP_OK);

        $baseUrl = Config::get('app.url') . '/api/v1/search/book?keyword=دولت';

        Config::set('cache.lifetime', 1);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', $baseUrl);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_required_keyword_for_search()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => 'password'
        ]);

        $token = $response->decodeResponseJson()['data']['access_token'];

        $response->assertStatus(Response::HTTP_OK);

        $baseUrl = Config::get('app.url') . '/api/v1/search/book';

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$token
        ])->json('POST', $baseUrl);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_required_jwt_token_for_search()
    {
        $baseUrl = Config::get('app.url') . '/api/v1/search/book?keyword=دولت';

        $response = $this->json('POST', $baseUrl);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
