<?php

namespace Tests\Feature\Api\V1\Auth;

use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /*
     * Login test
     * */

    public function test_successful_login()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => 'password'
        ]);

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'access_token', 'token_type', 'expires_in'
                ]
            ]);
    }

    public function test_required_email_and_password_for_login()
    {
        $response = $this->login();

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['email', 'password']);
    }

    public function test_required_email_for_login()
    {
        $response = $this->login([
            'email' => '',
            'password' => 'password'
        ]);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['email']);
    }

    public function test_required_valid_email_for_login()
    {
        $response = $this->login([
            'email' => 'test@',
            'password' => 'password'
        ]);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['email']);
    }

    public function test_required_password_for_login()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => ''
        ]);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors(['password']);
    }

    public function test_cannot_login_with_incorrect_email()
    {
        $response = $this->login([
            'email' => 'notfound@email.com',
            'password' => 'password'
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_cannot_login_with_incorrect_password()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /*
     * view account test
     * */

    public function test_user_can_view_account_info()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => 'password'
        ]);

        $token = $response->decodeResponseJson()['data']['access_token'];

        $response->assertStatus(Response::HTTP_OK);

        $baseUrl = Config::get('app.url') . '/api/v1/me';

        $response = $this->withHeaders(['Authorization' => 'Bearer '.$token])->json('POST', $baseUrl);
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'id', 'name', 'email'
                ]
            ]);
    }

    public function test_required_jwt_token_for_user_can_view_account_info()
    {
        $baseUrl = Config::get('app.url') . '/api/v1/me';

        $response = $this->json('POST', $baseUrl);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /*
     * Logout test
     * */

    public function test_successful_logout()
    {
        $response = $this->login([
            'email' => 'test@email.com',
            'password' => 'password'
        ]);

        $token = $response->decodeResponseJson()['data']['access_token'];

        $response->assertStatus(Response::HTTP_OK);

        $baseUrl = Config::get('app.url') . '/api/v1/logout';

        $response = $this->withHeaders(['Authorization' => 'Bearer '.$token])->json('POST', $baseUrl);
        $response->assertStatus(Response::HTTP_OK);

        $baseUrl = Config::get('app.url') . '/api/v1/me';

        $response = $this->withHeaders(['Authorization' => 'Bearer '.$token])->json('POST', $baseUrl);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_required_jwt_token_for_logout()
    {
        $baseUrl = Config::get('app.url') . '/api/v1/logout';

        $response = $this->json('POST', $baseUrl);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
