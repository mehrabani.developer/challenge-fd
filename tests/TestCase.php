<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Config;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    protected $seed = true;

    protected function login($data = [])
    {
        $baseUrl = Config::get('app.url') . '/api/v1/login';

        return $this->json('POST', $baseUrl, $data);
    }
}
